const express = require("express");
const homeController = require("../controllers/homeController.js");
const homeRouter = express.Router();


homeRouter.use("/", homeController.index);

module.exports = homeRouter;