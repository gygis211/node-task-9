const express = require("express");
const userController = require("../controllers/userController");
const userRouter = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: true });
const auth = require("../auth");

userRouter.post("/create", urlencodedParser, userController.addUser);
userRouter.post("/register", urlencodedParser, userController.regUser);
userRouter.post("/login", urlencodedParser, userController.loginUser);
userRouter.get("/:id", urlencodedParser, auth.verifyToken, userController.getUsers);
userRouter.delete("/:id", urlencodedParser, auth.verifyToken, userController.deleteUser);
userRouter.put("/:id", urlencodedParser, auth.verifyToken, userController.putUser);

module.exports = userRouter;