const express = require("express");
const roomsController = require("../controllers/roomController");
const roomsRouter = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: true });
const auth = require("../auth");

roomsRouter.post("/create", urlencodedParser, auth.verifyToken, auth.isAdmin, roomsController.addRoom);
roomsRouter.get("/allmyrooms", urlencodedParser, auth.verifyToken, roomsController.getRooms);
roomsRouter.get("/:id", urlencodedParser, auth.verifyToken, roomsController.getRoomById);
roomsRouter.put("/update", urlencodedParser, auth.verifyToken, auth.isAdmin, roomsController.updateRoom);
roomsRouter.delete("/delete", urlencodedParser, auth.verifyToken, auth.isAdmin, roomsController.deleteRoom);

module.exports = roomsRouter;