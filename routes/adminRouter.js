const express = require("express");
const adminController = require("../controllers/adminController");
const adminRouter = express.Router();
const bodyParser = require('body-parser');
const auth = require("../auth");

adminRouter.post("/create", bodyParser.json(), adminController.addAdmin);
adminRouter.post("/register", adminController.regAdmin);
adminRouter.post("/login", adminController.loginAdmin);
adminRouter.get("/:id", bodyParser.json(), auth.verifyToken, adminController.getAdmins);
adminRouter.delete("/:id", bodyParser.json(), auth.verifyToken, adminController.deleteAdmin);
adminRouter.put("/:id", bodyParser.json(), auth.verifyToken, adminController.putAdmin);

module.exports = adminRouter;