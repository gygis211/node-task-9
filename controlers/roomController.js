const Admin = require('../models/admin');
const Room = require('../models/room');
const User = require('../models/user');
const db = require('../db');
const mongoose = require("mongoose");

const addRoom = async (req, res) => {
    try {
        const admin = await Admin.findById(req.decoded.admin_id);
        const room = await Room.create({ number: req.body.number, admin: req.decoded.admin_id, users: req.body.users });
        admin.rooms.push(room._id);
        await admin.save();
        await User.updateMany({ _id: req.body.users }, { $push: { rooms: room._id } });
        res.send(room);
    }
    catch (error) {
        console.log(error);
        res.status(403).send("oh no");
    }

};

const getRooms = async (req, res) => {
    try {
        if (req.decoded.admin_id) {
            const createdRoomsAdmin = await Room.find({ admin: { $eq: req.decoded.admin_id } });
            res.send(createdRoomsAdmin);
        } else if (req.decoded.user_id) {
            const createdRoomsUser = await Room.find({ users: req.decoded.user_id });
            const rooms = createdRoomsUser.map(({ _id, number }) => ({ _id, number }));
            res.send(rooms);
        }
    }
    catch (error) {
        res.status(403).send("oh no");
    }
}

const getRoomById = async (req, res) => {
    try {
        if (req.decoded.admin_id) {

            const getAdminIdRoom = await Room.findById(req.params.id);
            res.send(getAdminIdRoom);
        } else if (req.decoded.user_id) {
            const getUserIdRoom = await Room.find().where('_id').equals(req.body.id);
            const userid = req.decoded.user_id;
            const createdRoomsUser = await (getUserIdRoom[0].users.includes(userid)
                ? res.send(getUserIdRoom)
                : res.status(403).send("You don't have permissions for this room"));
            res.send(createdRoomsUser);
        }
    }
    catch (error) {
        console.log(error);
        res.status(403).send("oh no");
    }
}

const updateRoom = async (req, res) => {
    try {
        const getAdminIdRoom = await Room.findById(req.body.id);
        if (req.decoded.admin_id == getAdminIdRoom.admin) {
            const users = req.body.users;
            const updatedRoom = await Room.findByIdAndUpdate(req.body.id, { number: req.body.number, users: users })
            res.send(updatedRoom);
        } res.status(403).send("Need other admin for change this room")
    }
    catch (error) {
        res.status(403).send("oh no");
    }

};

const deleteRoom = async (req, res) => {
    try {
        console.log(await mongoose.startSession);
        const getAdminIdRoom = await Room.findById(req.body.id);
        if (req.decoded.admin_id == getAdminIdRoom.admin) {
            const session = await mongoose.startSession();
            await session.withTransaction(async () => {
                const deletedRoom = await (await Room.findByIdAndDelete(req.body.id));
                await Admin.updateOne({ _id: req.decoded.admin_id }, { $pull: { rooms: deletedRoom._id } });
                await User.updateMany({}, { $pull: { rooms: deletedRoom._id } });
                res.send(deletedRoom);
            })
        } else { res.status(403).send("Need other admin for delete this room") }
    }
    catch (error) {
        console.log(error);
        res.status(403).send("oh no");
    }

};

module.exports = { addRoom, getRooms, getRoomById, updateRoom, deleteRoom };