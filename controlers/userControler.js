const User = require('../models/user')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const accessToken = require('../config');

const addUser = function (req, res) {
    User.create({ name: req.body.name, age: req.body.age, login: req.body.login, password: req.body.password }, function (error, doc) {
        if (error) return console.log(error);
        res.send(doc);
    });
};

const getUsers = function (req, res) {
    User.find({ _id: req.params.id }, function (error, doc) {
        if (error) return console.log(error);
        res.send(doc);
    })
};

const deleteUser = (req, res) => {
    User.findOneAndDelete({ _id: req.params.id }, function (error, doc) {
        if (error) return console.log(error);
        res.send(doc);
    })
};

const putUser = function (req, res) {
    User.findByIdAndUpdate({ _id: req.params.id }, { name: req.body.name, age: req.body.age }, function (error, user) {
        if (error) return console.log(error);
        res.send(user);
    })
};

const regUser = async (req, res) => {
    try {
        const { name, age, login, password } = req.body;
        if (!(login && password && age && name)) {
            res.status(400).send("All input is required");
        }
        const oldUser = await User.findOne({ login });
        if (oldUser) {
            return res.status(409).send("User Already Exist. Please Login");
        }
        encryptedPassword = await bcrypt.hash(password, 10);
        const user = await User.create({
            name,
            age,
            login: login.toLowerCase(),
            password: encryptedPassword,
        });
        const token = jwt.sign(
            { user_id: user._id, login },
            accessToken,
            {
                expiresIn: "2h",
            }
        );
        user.token = token;
        res.status(201).json(user);
    } catch (error) {
        console.log(error);
    }
};

const loginUser = async (req, res) => {
    try {
        const { login, password } = req.body;
        if (!(login && password)) {
            res.status(400).send("All input is required");
        }
        const user = await User.findOne({ login });
        if (user && (await bcrypt.compare(password, user.password))) {
            const token = jwt.sign(
                { user_id: user._id, login },
                accessToken,
                {
                    expiresIn: "2h",
                }
            );
            user.token = token;
            res.status(200).json(user);
        }
        res.status(400).send("Invalid Credentials");
    } catch (error) {
        console.log(error);
    }
};

module.exports = { deleteUser, putUser, getUsers, addUser, regUser, loginUser };