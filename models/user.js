const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
      type: String,
      default: "NoName",
      minlength: 2,
      maxlength: 50
    },
  age: {
    type: Number,
    default: 18,
    min: 1,
    max:150
  },
  login: { type: String, unique: true },
  password: { type: String },
  token: { type: String },
  status: {
    type: String,
    default: "User"
  },
  rooms:[{
    type: mongoose.Schema.Types.ObjectId,  
    ref: 'Room',
    autopopulate: true
  }]
}, { versionKey: false });

const User = mongoose.model("User", UserSchema);
UserSchema.plugin(require('mongoose-autopopulate'));

module.exports = User;