const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RoomSchema = new Schema({
    number: {
        type: Number,
        unique: true,
        required: true
    },
    users: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        autopopulate: true
    }],
    admin: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin",
        autopopulate: true
    }
},{ versionKey: false });

RoomSchema.pre('find', function () {
    this.populate('users');
})
const Room = mongoose.model("Room", RoomSchema);
RoomSchema.plugin(require('mongoose-autopopulate'));

module.exports = Room;
