const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AdminSchema = new Schema({
  name: {
      type: String,
      default: "NoName",
      required: true,
      minlength: 2,
      maxlength: 50
    },
    age: {
      type: Number,
      default: 18,
      required: true,
      min: 1,
      max:150
    },
    login: { type: String, unique: true },
    password: { type: String },
    token: { type: String },
    status: {
      type: String,
      default: "Admin"
    },
    rooms: [{
      type: mongoose.Schema.Types.ObjectId,  
      ref: 'Room',
      autopopulate: true
    }]
  },{ versionKey: false });

const Admin = mongoose.model("Admin", AdminSchema);
AdminSchema.plugin(require('mongoose-autopopulate'));

module.exports = Admin;